/**
 * Created by Mateusz on 2016-04-07.
 */

angular.module('calendar')
    .controller('calendarController', ['$scope', 'SendDataService', 'GetDataService', 'Calendar',
            function($scope, SendDataService, GetDataService, Calendar){
                    $scope.polishMonths = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj",
                            "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];
                    $scope.days = [];
                    $scope.year = moment().year();
                    $scope.month = moment().month();
                    $scope.day = moment().day();
                    $scope.currentDay = moment();
                    var pickedDates ={
                            start : "",
                            ending: "",
                            count: 0,
                            startIndex: 0,
                            endingIndex: 0
                    };

                    $scope.pickDate = function (item) {
                            if(pickedDates.count==0 ){
                                    console.log($scope.currentDay.format("YYYY-MM-DD"));
                                    pickedDates.start = moment(item.day.saveFormat, "YYYY-MM-DD");
                                    pickedDates.count++;
                                    pickedDates.startIndex = item.$index;
                                    $scope.days[pickedDates.startIndex].selected = true;
                            } else if(item.$index < pickedDates.startIndex && pickedDates.count < 2){
                                    pickedDates.start = moment(item.day.saveFormat, "YYYY-MM-DD");
                                    $scope.days[pickedDates.startIndex].selected = false;
                                    pickedDates.startIndex = item.$index;
                                    selectDatesRange(pickedDates.startIndex, pickedDates.endingIndex);
                            }
                            else {
                                    pickedDates.endingIndex = item.$index;
                                    if(pickedDates.endingIndex > pickedDates.startIndex){
                                            pickedDates.ending = moment(item.day.saveFormat, "YYYY-MM-DD");
                                            selectDatesRange(pickedDates.startIndex, pickedDates.endingIndex);
                                    }
                            }
                    };

                    function selectDatesRange(start, end){
                            $scope.days = Calendar.createCurrentMonth($scope.year, $scope.month);
                            for(var i = start; i <=end; i++){
                                    console.log("selecting");
                                    $scope.days[i].selected = true;
                            }
                    }

                    $scope.getdates = function(){
                            Calendar.checkBookedTerms();
                    }

                    $scope.book = function(){
                            if(Calendar.checkIfBooked(pickedDates.start._i, pickedDates.ending._i)){
                                    SendDataService.bookTerm(pickedDates);
                                    Calendar.checkBookedTerms();
                                    alert("Rezerwacja udana");
                            } else{
                                    alert("Termin już zajęty");
                            }
                            $scope.reset();
                    };

                    $scope.previousMonth = function(){
                            if($scope.month==0){
                                    $scope.year = $scope.year - 1;
                                    $scope.month = 12;
                            }
                            $scope.month = $scope.month - 1;
                            $scope.days = Calendar.createCurrentMonth($scope.year, $scope.month);
                    };

                    $scope.nextMonth = function(){
                            if($scope.month==11){
                                    $scope.year = $scope.year + 1;
                                    $scope.month = -1;
                            }
                            $scope.month = $scope.month + 1;
                            $scope.days = Calendar.createCurrentMonth($scope.year, $scope.month);
                    };

                    $scope.reset = function(){
                            $scope.days = Calendar.createCurrentMonth($scope.year, $scope.month);
                            pickedDates ={
                                    start : "",
                                    ending: "",
                                    count: 0,
                                    startIndex: 0,
                                    endingIndex: 0
                            };
                    }

                    $scope.reset();
                    $scope.$watch('days', function () {});
            }]);