/**
 * Created by Matt on 2016-04-07.
 */

angular.module('calendar')
    .service('Calendar', ['GetDataService', function (GetDataService) {
        var days = [];
        var bookedDays = checkBookedTerms() || [];

        function checkBookedTerms(){
            GetDataService.getBookedTerms().then(function(response){
                bookedDays = response.data;
                return response.data;
            }, function(response){
                return response;
            });
        }

        function checkIfBooked(start, end){
            if(bookedDays.length){
                for(var i = 0; i < bookedDays.length; i++){
                    if(moment(start).isBetween(bookedDays[i].startdate, bookedDays[i].endingdate) || moment(end).isBetween(bookedDays[i].startdate, bookedDays[i].endingdate) ){
                        return false;
                    }
                }
            }
            return true;
        }


        function createWeeksInCurrentMonth(year, month, firstDayOfTheMonth, lastDayOfTheMonth){
            var days = [];
            var lastDay = parseInt(moment().year(year).month(month-1).endOf('month').format("DD)"));
            if(firstDayOfTheMonth==0 && lastDay==30)
            {
                firstDayOfTheMonth = 7;
            }

            for(var i =  lastDay - firstDayOfTheMonth + 1; i < lastDay; i++){

                var dayToPush = dayToPushCreator(year, month-1, i+1, false);
                days.push(dayToPush);
            }

            var lastDay2 =  parseInt(moment().month(month).endOf('month').format("DD)"));

            for(var j = 1; j <= lastDay2; j++){
                var dayToPush = dayToPushCreator(year, month, j, true);
                days.push(dayToPush);
            }

            for(var k = days.length, i =1; k < 42; k++, i++){
                if(month==11){
                    month=-1;
                    year++;
                }
                var dayToPush = dayToPushCreator(year, month, i, false);
                days.push(dayToPush);
            }

            function dayToPushCreator(year, month, date, current){
                return  {
                    dayNumber: moment().year(year).month(month).date(date).format("DD"),
                    monthNumber: moment().year(year).month(month).date(date).format("MM"),
                    display: moment().year(year).month(month).date(date).format("DD-MM"),
                    saveFormat: moment().year(year).month(month).date(date).format("YYYY-MM-DD"),
                    inCurrentMonth : current,
                    year: year
                };
            }
            return days;
        }

        function createCurrentMonth(year, month){
            var firstDayOfTheMonth = parseInt(moment().year(year).month(month).days());
            var lastDayOfTheMonth = parseInt(moment().year(year).month(month).endOf('month').format("DD)"));
            days = createWeeksInCurrentMonth( year, month, firstDayOfTheMonth, lastDayOfTheMonth);
            return days;
        }


        return{
            createWeeksInCurrentMonth: createWeeksInCurrentMonth,
            createCurrentMonth: createCurrentMonth,
            checkIfBooked: checkIfBooked,
            checkBookedTerms: checkBookedTerms
        }
    }]);