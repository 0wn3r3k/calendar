/**
 * Created by Mateusz on 2016-04-07.
 */
angular.module('calendar')
    .service('SendDataService', ['$http', function($http) {
        this.SendDataService = {};

        this.SendDataService.bookTerm = function(pickedDates){
            console.log("SendDataService working");
            return $http({
                url: "../ajax/bookTerm.php",
                method: "POST",
                data:{start: pickedDates.start._i, end: pickedDates.ending._i}
            }).success(function(data) {
                return data;
            }).error(function(data, status) {
                return status;
            });
        };
        return this.SendDataService;

    }]);