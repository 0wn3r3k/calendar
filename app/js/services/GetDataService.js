/**
 * Created by Mateusz on 2016-04-07.
 */
angular.module('calendar')
    .service('GetDataService', ['$http', function($http) {
        this.GetDataService = {};

        this.GetDataService.getBookedTerms = function(){
            return $http({
                url: "../ajax/getTerms.php",
                method: "GET"
            }).success(function(data) {
                return data;
            }).error(function(data, status) {
                return status;
            });
        };
        return this.GetDataService;

    }]);